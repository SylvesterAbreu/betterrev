package org.adoptopenjdk.betterrev.controllers;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RequestScoped
@Path("authentication")
public class AuthenticationController {

    // TODO - make these configurable and change these to the "official" BetterRev Bitbucket key/secret
    private static final String KEY = "";
    private static final String SECRET = "";

    @Context
    private UriInfo uriInfo;

    public ServiceBuilder setupOAuthService() {
        return new ServiceBuilder()
                .provider(BitBucketApi.class)
                .apiKey(KEY)
                .apiSecret(SECRET);
    }

    @GET
    @Path("signin")
    @Produces(MediaType.APPLICATION_JSON)
    public Response signIn() throws URISyntaxException {
        final URI callbackUri = resolveCallbackUri();
        final OAuthService oAuthService = setupOAuthService().callback(callbackUri.toString()).build();
        final Token requestToken = oAuthService.getRequestToken();
        final String authenticationUrl = oAuthService.getAuthorizationUrl(requestToken);
        final Map<String, String> response = new HashMap<String, String>() {{
            put("authenticationUrl", authenticationUrl);
            put("oauthSecret", requestToken.getSecret());
        }};
        return Response.ok(response).build();
    }

    private URI resolveCallbackUri() {
        final String requestPath = uriInfo.getRequestUri().getPath();
        final int applicationPathIndex = requestPath.indexOf("betterrev/");
        final String baseRequestPath = requestPath.substring(0, applicationPathIndex);
        return uriInfo.getRequestUriBuilder().replacePath(baseRequestPath + "authenticate.html").build();
    }

    @GET
    @Path("authenticate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticate(
            @QueryParam("oauth_token") String token,
            @QueryParam("oauth_verifier") String verifier,
            @QueryParam("oauth_secret_token") String secretToken) {
        final OAuthService oAuthService = setupOAuthService().debug().build();
        final Token accessToken = oAuthService.getAccessToken(new Token(token, secretToken), new Verifier(verifier));
        final Map<String, String> response = Collections.singletonMap("oauthAccessToken", accessToken.getToken());
        return Response.ok(response).build();
    }
}
