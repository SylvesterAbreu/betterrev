betterrevApp.directive('signIn', ['authenticationService', function (authenticationService) {

    var template = '<button type="button" class="btn btn-primary navbar-btn" ng-click="authenticate()">Sign In</button>';

    return {
        restrict: 'A',
        template: template,
        scope: {},
        link: function (scope, element, attrs) {

            scope.authenticate = function () {
                authenticationService.authenticate();
            }
        }
    };
}]);