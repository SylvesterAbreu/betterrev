var chai = require('chai');
var expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('Tags Page Tests', function () {

    beforeEach(function () {
        browser.get('http://127.0.0.1:8090/#/admin/tags');
    });

    it('Tags page elements should be present', function () {
        expect(element(by.id("tagsTable")).isDisplayed()).to.eventually.equal(true);
        expect(element(by.id("addTag")).isDisplayed()).to.eventually.equal(true);
    });

    it('Clicking the add new tag button should display the expected modal window', function () {
        element(by.id("addTag")).click();
        expect(element(by.className('modal-content')).isDisplayed()).to.eventually.equal(true);
    });

    it('Creating a tag with an empty name should be highlighted when submitting', function () {
        element(by.id("addTag")).click();
        element(by.id("modalMainAction")).click();
        expect(element(by.className("has-error")).isPresent()).to.eventually.equal(true);
    });
});